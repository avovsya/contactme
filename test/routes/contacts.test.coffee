request = require 'supertest'
app = require '../../app.coffee'

describe 'GET /contacts', ->
    it 'respond with json', (done) ->
        request(app)
            .get('/contacts')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done)
