// Generated by CoffeeScript 1.4.0
(function() {
  'use strict';

  window.app.controller('ContactController', [
    '$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {
      $scope.getContact = function(id) {
        return $http.get("/api/contacts/" + id).success(function(data) {
          return $scope.contact = data;
        });
      };
      $scope.updateContact = function() {
        if ($scope.updateTimeout != null) {
          clearTimeout($scope.updateTimeout);
        }
        return $scope.updateTimeout = setTimeout((function() {
          $http.put("/api/contacts/edit/" + $scope.contact.id, $scope.contact);
          return clearTimeout($scope.updateTimeout);
        }), 2000);
      };
      $scope.getContact($routeParams.id);
      return $scope.$on('query-changed', function(event, args) {
        var field, newValue, _ref;
        field = (_ref = /(^[a-zA-Z0-9_]*):/.exec(args.query)) != null ? _ref[1] : void 0;
        newValue = (args.query.replace(/(^[a-zA-Z0-9_]*:)/, '')).trim();
        if (field !== 'id' && $scope.contact.hasOwnProperty(field)) {
          $scope.contact[field] = newValue;
          return $scope.updateContact();
        }
      });
    }
  ]);

}).call(this);
