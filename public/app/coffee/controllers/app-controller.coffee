'use strict'

window.app.controller 'AppController',
    ['$scope', '$route', '$routeParams',
        ($scope, $route, $routeParams) ->
            $scope.subview = "contact"

            renderRoot = ->
                # render root path
                # if not logged in - login page
                # else - contact list
                $scope.subview = 'contacts'
                $scope.renderPath[1] = 'list'

            
            render = ->
                $scope.renderPath = $route.current.action.split '.'
                if $scope.renderPath[0] is 'root'
                    renderRoot()
                else
                    $scope.subview = $scope.renderPath[0]

            $scope.$on '$routeChangeSuccess', ($currRoute, $prevRoute) ->
                return render() if $route.current.action?
                $scope.subview = '404'
                
    ]
