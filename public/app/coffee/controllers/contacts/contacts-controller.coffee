'use strict'

window.app.controller 'ContactsController',
    ['$scope', '$http', '$routeParams',
    ($scope, $http, $routeParams) ->
        $scope.panelMode = "text-control"

        $scope.contactSubview = 'list'

        $scope.$watch 'renderPath', ->
            $scope.contactSubview = $scope.renderPath[1]

    ]
