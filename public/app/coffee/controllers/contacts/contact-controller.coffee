'use strict'

window.app.controller 'ContactController',
    ['$scope', '$http', '$routeParams',
    ($scope, $http, $routeParams) ->

        $scope.getContact = (id) ->
            $http.get("/api/contacts/#{id}")
                .success (data) ->
                    $scope.contact = data

        $scope.updateContact = ->
            if $scope.updateTimeout?
                clearTimeout($scope.updateTimeout)
            $scope.updateTimeout = setTimeout (->
                $http.put("/api/contacts/edit/#{$scope.contact.id}", $scope.contact)
                clearTimeout($scope.updateTimeout)), 2000

        $scope.getContact($routeParams.id)

        $scope.$on 'query-changed', (event, args) ->
            field = /(^[a-zA-Z0-9_]*):/.exec(args.query)?[1]
            newValue = (args.query.replace /(^[a-zA-Z0-9_]*:)/, '').trim()

            if field isnt 'id' and $scope.contact.hasOwnProperty(field)
                $scope.contact[field] = newValue
                $scope.updateContact()
    ]
