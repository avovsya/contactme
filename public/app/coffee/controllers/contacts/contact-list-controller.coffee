'use strict'

window.app.controller 'ContactListController',
    ['$scope', '$http', ($scope, $http) ->

        $scope.getContactList = (filter) ->
            if not filter? or filter.length <= 0
                $http.get('/api/contacts')
                    .success (data) ->
                        $scope.contacts = data
            else
                $http.get("/api/contacts/filter/#{filter}")
                    .success (data) ->
                        $scope.contacts = data

        $scope.getContactList()

        $scope.$on 'query-changed', (event, args) ->
            $scope.getContactList args.query
    ]
