'use strict'

window.app = angular.module('app', [])

window.app.config ['$routeProvider',
        ($routeProvider) ->
            $routeProvider
                .when('/',
                    action: 'root'
                )
                .when('/contacts',
                    action: 'contacts.list'
                )
                .when('/contacts/:id',
                    action: 'contacts.show'
                )
                .when('/settings',
                    action: 'contacts.settings'
                )
                .when('/login',
                    action: 'login'
                )
                .when('/register',
                    action: 'register'
                )
                .otherwise
                    redirectTo: '/'
    ]
