Faker = require 'Faker'

mockContact = (n) ->
    contact =
        id: n
        name:
            firstName: Faker.Name.firstName()
            lastName: Faker.Name.lastName()
            nickname: Faker.Internet.userName()
        organization: Faker.Company.companyName()
        urls: [ Faker.Internet.domainName() ]
        emails: [ Faker.Internet.email() ]
        phones: [ Faker.PhoneNumber.phoneNumber() ]
        addresses: [
            {
                streetAddress: Faker.Address.streetAddress()
                city: Faker.Address.city()
                state: Faker.Address.usState()
                zipCode: Faker.Address.zipCode()
                country: Faker.Address.ukCountry()
            }
        ]
        bday: new Date(1985, 5,13)
        category: Faker.Lorem.words()
        note: Faker.Lorem.sentence()

module.exports = (n) ->
    res = []
    for num in [1..n]
        res.push mockContact(num)

    return res
            

