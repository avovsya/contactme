_ = require 'lodash'

contacts = require('../data/testContacts') 100

module.exports =
    edit: (req, res) ->
        contact = _.find contacts, (c) -> c.id is parseInt(req.params.id)
        contact.name = req.body.name
        contact.phone = req.body.phone

    filter: (req, res) ->
        query = new RegExp req.params.filter, 'i'
        res.send _.filter contacts, (c) -> query.test c.name

    getContact: (req, res) ->
        res.send _.find contacts, (c) -> c.id is parseInt(req.params.id)

    getAllContacts: (req, res) ->
        res.send contacts

        
