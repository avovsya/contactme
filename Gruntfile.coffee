{spawn} = require 'child_process'
which = require('which').sync

module.exports = (grunt) ->
    REPORTER = 'dot'

    grunt.loadNpmTasks 'grunt-reload'
    grunt.loadNpmTasks 'grunt-regarde'
    grunt.loadNpmTasks 'grunt-simple-mocha'

    outputStdout = (data) ->
        console.log data.toString('utf8').trim()

    output = (proc) ->
        proc.stdout.on 'data', (data) -> outputStdout data
        proc.stderr.on 'data', (data) -> outputStdout data

    grunt.config.init

        reload:
            port: 6001
            proxy:
                host: 'localhost'
                port: 3000
        regarde:
            tests:
                files: ['test/**/*.coffee', 'lib/**/*.coffee', 'app.coffee', 'test/**/*.js', 'lib/**/*.js']
                tasks: ['simplemocha']
                # spawn: true
            app:
                files: [
                    'public/app/*.html',
                    'public/app/styles/**/*.styl',
                    'public/app/styles/**/*.css',
                    'public/app/images/**/*',
                    'public/app/**/*.html',
                    # scripts
                    'public/app/coffee/**/*.coffee',
                    'public/app/scripts/**/*.js',
                    # tests
                    'public/test/spec/coffee/**/*.coffee'
                    'public/test/spec/**/*.js',
                ]
                tasks: 'regarde:trigger'
        simplemocha:
            options:
                timeout: 3000
                ignoreLeaks: false
                ui: 'bdd'
                reporter: 'spec'
                compilers: 'coffee:coffee-script'
            all:
                src: 'test/**/*.coffee'

    grunt.registerTask 'supervisor', ->
        cmd = which('nodemon')
        output spawn cmd, [
            '--watch',
            'lib',
            '--watch',
            'app.coffee',
            'app.coffee'
        ]

    grunt.registerTask 'nodev', ->
        cmd = which('nodev')
        output spawn cmd, [
            '--watch',
            'lib',
            '--watch',
            'app.coffee',
            'app.coffee'
        ]

    grunt.registerTask 'regarde:trigger', () ->
        grunt.regarde.changed.forEach (file) ->
            if /\.coffee$/.test file
                grunt.task.run 'coffee:compile'
            else if /\.styl$/.test file
                grunt.task.run 'stylus:compile'
            else grunt.task.run 'reload'

    grunt.registerTask 'stylus:compile', ->
        stylCmd = which('stylus')
        output spawn stylCmd, ['public/app/styles/main.styl', '--include-css']

    # compile clien-side coffee scripts(application and tests)
    grunt.registerTask 'coffee:compile', ->
        cmd = which('coffee')
        output spawn cmd, ['--compile', '--output', 'public/app/scripts/', 'public/app/coffee/']
        output spawn cmd, ['--compile', '--output', 'public/test/spec/', 'public/test/spec/coffee/']

    grunt.registerTask 'test', ['simplemocha']
    grunt.registerTask 'test:watch', ['simplemocha', 'regarde:tests']
    grunt.registerTask 'debug', ['nodev']
    grunt.registerTask 'server', ['coffee:compile','stylus:compile', 'supervisor', 'reload', 'regarde:app']
    grunt.registerTask 'default', ['server']

